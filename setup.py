﻿from setuptools import setup

with open('README.rst') as readme:
    long_description = readme.read()

setup(
    name='tsfuzzy',
    version='0.0.1',
    description='Sistemas de inferência fuzzy em Python',
    author='Lucas Hermann Negri',
    author_email='lucas.negri@sociesc.com.br',
    packages=['tsfuzzy'],
    classifiers=[
        'Development Status :: 4 - Beta',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3',
        'Topic :: Scientific/Engineering'
    ]
)
