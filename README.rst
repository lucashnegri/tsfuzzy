tsfuzzy
=======

Biblioteca para sistemas de inferência fuzzy Takagi-Sugeno para Python 3.

Funcionalidades:

- Definição do conjunto de regras de forma natural, utilizando os operadores em Python;
- Execução do sistema diretamente em Python;
- Geração de código em C a parir das regras definidas em Python, para execução em microcontroladores;

Já existem bibliotecas que implementam sistemas de inferência fuzzy para Python, como a scikit-fuzzy [1].
O diferencial da tsfuzzy é o foco na simplicidade e na geração de código otimizado para microcontroladores.

Instalação
----------

Para instalar o tsfuzzy a partir do pacote com o código fonte, digite:

.. code-block:: bash

    python setup.py install

tsfuzzy tem como alvo o Python 3.5 (pode funcionar em versões anteriores com pequenas alterações).

[1]: https://github.com/scikit-fuzzy
