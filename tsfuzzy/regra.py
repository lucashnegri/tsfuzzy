"""
Regra: SE "antecedente" ENTAO "consequente".
"""
class Regra:
    def __init__(self, antecedente, consequente):
        self.antecedente = antecedente
        self.consequente = consequente
        
    def __call__(self, norma, valores):
        """
        Avalia a regra, utilizando uma determinada `norma` os valores das
        entradas especificados, em ordem, por `valores`.
        """
        w = self.antecedente(norma)
        return w, self.consequente(w, valores)
    