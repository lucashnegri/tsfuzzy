import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
from tsfuzzy import pertinencia

def mostra_entrada(entrada):
    a, b = entrada.limites
    
    for nome, pert in entrada.pert.items():
        x = np.linspace(a, b, 200)
        entrada.valor = x
        plt.plot(x, pert(), label=nome)

    plt.ylim(-0.1, 1.1)
    plt.grid()
    plt.legend()
    plt.xlabel("Valor")
    plt.ylabel("Pertinência")
    plt.show()

def mostra_mapeamento2d(base):
    assert len(base.entradas) == 2
    x = np.linspace(*base.entradas[0].limites, 100)
    y = np.linspace(*base.entradas[0].limites, 100)
    
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    X, Y = np.meshgrid(x, y)
    zs = np.array([base([x, y]) for x, y in zip(np.ravel(X), np.ravel(Y))])
    Z = zs.reshape(X.shape)

    ax.plot_surface(X, Y, Z)
    ax.set_xlabel('Serviço')
    ax.set_ylabel('Comida')
    ax.set_zlabel('Gorgeta [%]')
    plt.show()
