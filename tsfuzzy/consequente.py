"""
Consequentes (se "antecedente" então "consequente").
"""
import numbers

def polinomial(coefs, valores=None):
    """
    Avalia o consequente pelo cálculo de um polinomio (usado em Takagi-Sugeno).
    O número de parâmetros em `coefs` dita a ordem.
    
    Parameters
    ----------
    coefs: np.array
        Coeficientes do polinômio, em ordem crescente de potência, intercalando as
        variáveis de entrada (ex.: [1, 0.2, 0.3, 2, 3]) resulta em:
        f(x) = 1 + 0.2*x[0] + 0.3*x[1] + 2*x[0]² + 3*x[1]²
    valores : np.array (opcional para ordem 0)
        Valores de entrada nos quais o polinômio será avaliado
    
    Returns
    -------
    float:
        Valor do polinômio
    """
    if isinstance(coefs, numbers.Real):
        return coefs
    else:
        acc = coefs[0]
        valores_potencia = valores.copy()
        n_valores = len(valores)
        
        for i in range(1, len(coefs), n_valores):
            acc += sum(valores_potencia * coefs[i : i + n_valores])
            valores_potencia *= valores
        
        return acc
    
class Consequente:
    pass
        
class Polinomial(Consequente):
    """
    Consequente polinomial (Takagi-Sugeno).
    Nota: na base de regras, os coeficientes correspondem a todas as entradas, e não somente
    aquelas presentes na regra em questão.
    """
    def __init__(self, coefs):
        self.coefs = coefs
    
    def __call__(self, peso, valores):
        return peso * polinomial(self.coefs, valores)
        
