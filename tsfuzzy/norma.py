"""
Normas T (triangulares) e normas S (conormas T).
"""
import numpy as np

def minimo(a, b):
    """T - min(a,b)."""
    return np.minimum(a, b)

def maximo(a, b):
    """S - max(a,b)."""
    return np.maximum(a, b)
    
def produto_algebrico(a, b):
    """T - a.b."""
    return a*b

def soma_probabilistica(a, b):
    """S - a+b - a.b."""
    return a+b - a*b
    
class Norma:
    def __init__(self, t, s):
        self.T = t
        self.S = s
        
    def __call__(self, tipo, a, b):
        return getattr(self, tipo)(a, b)
    
MinMax = Norma(minimo, maximo)
ProdSoma = Norma(produto_algebrico, soma_probabilistica)