"""
Entrada do sistema de inferência. Comporta as funções de pertinência.
"""
import numpy as np
import collections

class Entrada:
    def __init__(self, inf=0, sup=10):
        self._inf = inf
        self._sup = sup
        self._valor = 0
        self.pert = collections.OrderedDict()
        
    @property
    def limites(self):
        return self._inf, self._sup
        
    @property
    def valor(self):
        return np.clip(self._valor, self._inf, self._sup)
    
    @valor.setter
    def valor(self, v):
        self._valor = v
    
    def __setitem__(self, k, v):
        """Configura uma função de pertinência."""
        v.entrada = self
        self.pert[k] = v
    
    def __getitem__(self, k):
        """Acessa uma função de pertinência."""
        return self.pert[k]
