"""
Base de regras, que agrega todas as regras do sistema e implementa o método de
inferência.
"""
import numpy as np
from tsfuzzy import norma, regra, consequente

class Base:
    def __init__(self, entradas, norma=norma.MinMax):
        self.entradas = entradas
        self.norma = norma
        self.regras = []

    def __call__(self, valores=None):
        # valores podem ser passados aqui ou pegos das entradas
        if valores:
            for entrada, valor in zip(self.entradas, valores):
                entrada.valor = valor
        else:
            valores = [e.valor for e in self.entradas]

        valores = np.array(valores, ndmin=1, copy=False)

        # inferência por Takagi-Sugeno
        numerador = 0
        denominador = 1e-12

        for r in self.regras:
            w, z = r(self.norma, valores)
            numerador += z
            denominador += w

        return numerador / denominador

    def adiciona(self, SE, ENTAO):
        if not isinstance(ENTAO, consequente.Consequente):
            ENTAO = consequente.Polinomial(np.array(ENTAO, ndmin=1, copy=False))

        self.regras.append(regra.Regra(SE, ENTAO))
