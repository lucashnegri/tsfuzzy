"""
Funções de pertinência (para a etapa de fuzzificação).
"""
import numpy as np
from tsfuzzy import antecedente

def trapezoidal(x, a, b, c, d):
    """
    Função de pertinência trapezoidal.
    
    Parameters
    ----------
    x : float ou np.array
        Valores para os quais a função de pertinência será avaliada.
    a: float
        Início da subida
    b: float
        Término da subida
    c: float
        Início da descida
    d: float
        Término da descida
    
    Returns
    -------
    float ou np.array
        Valores de pertiência calculados
    """
    return np.clip(np.minimum((x-a)/(b-a), (d-x)/(d-c)), 0, 1)
    
def triangular(x, a, b, c):
    """
    Função de pertinência triangular.
    
    Parameters
    ----------
    x : float ou np.array
        Valores para os quais a função de pertinência será avaliada.
    a: float
        Início da subida
    b: float
        Incício da descida
    c: float
        Término da descida
    
    Returns
    -------
    float ou np.array
        Valores de pertiência calculados
    """
    return trapezoidal(x, a, b, b, c)

class Pertinencia(antecedente.Antecedente):
    def __init__(self, *args):
        self.params = args
        self.entrada = None
    
    def __call__(self, x=None):
        if self.entrada is not None:
            x = self.entrada.valor
        
        return self.__class__.func(x, *self.params)
    
    def limites(self):
        return self.params[0], self.params[-1]

class Trapezoidal(Pertinencia):
    func = trapezoidal
    
class Triangular(Pertinencia):
    func = triangular
