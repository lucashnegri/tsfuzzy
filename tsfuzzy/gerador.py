"""
Implementa a geração de código em C.
"""
import tsfuzzy as fz

T_BIBLIOTECA = """#include <stdio.h>
 
/* Funções de pertinência */

float trapezoidal(float x, float a, float b, float c, float d) {
    float val = x <= b ? (x - a) / (b - a) : (d - x) / (d - c);
    if(val < 0) val = 0;
    if(val > 1) val = 1;
    return val;
}

float triangular(float x, float a, float b, float c) {
    return trapezoidal(x, a, b, b, c);
}

/* Normas */

float minimo(float a, float b) {
    return a < b ? a : b;
}

float maximo(float a, float b) {
    return a > b ? a : b;
}

float produto_algebrico(float a, float b) {
    return a * b;
}

float soma_probabilistica(float a, float b) {
    return a + b - a * b;
}

"""

def atribui_identificadores(base):
    # atribui um id único para cada entrada e para cada função de pertinência
    for i, entrada in enumerate(base.entradas, 1):
        entrada.id = i

        for j, pert in enumerate(entrada.pert.values(), 1):
            pert.id = j

    # atribui id único a cada regra
    for i, regra in enumerate(base.regras, 1):
        regra.id = i

def gera_entrada(entrada, f):
    T_PERT = 'float ent_{}_pert_{} = {}(entrada_{}, {});\n'
    f.write('/* Entrada {} */\n'.format(entrada.id))
    
    for pert in entrada.pert.values():
        f.write(T_PERT.format(entrada.id, pert.id, pert.func.__name__,
                              entrada.id, ', '.join(map(str, pert.params))))
    
    f.write('\n')
    
def gera_antecedente(antecedente, norma):
    if isinstance(antecedente, fz.Pertinencia):
        return 'ent_{}_pert_{}'.format(antecedente.entrada.id, antecedente.id)
    else:
        T_CHAMADA = '{}({}, {})'

        return T_CHAMADA.format(
            getattr(norma, antecedente.tipo).__name__,
            gera_antecedente(antecedente.esquerda, norma),
            gera_antecedente(antecedente.direita, norma)
        )

def gera_consequente(consequente, n_entradas):
    assert len(consequente.coefs) <= n_entradas + 1, \
        "Somente constantes e termos lineares são suportados nos consequentes"
    assert len(consequente.coefs) in (1, 1 + n_entradas), \
        "Número inválido de coeficientes"
    termos = [str(consequente.coefs[0])]
    
    for i, coef in enumerate(consequente.coefs[1:], 1):
        termos.append("entrada_{} * {}".format(i, coef))
        
    return ' + '.join(termos)

def gera_regra(regra, norma, n_entradas, f):
    T_ANTECEDENTE = 'float antecedente_regra_{} = {};\n'
    T_CONSEQUENTE = 'float consequente_regra_{} = {};\n'
    T_SOMA_NUM = 'num += antecedente_regra_{} * consequente_regra_{};\n'
    T_SOMA_DEN = 'den += antecedente_regra_{};\n'

    f.write('/* Regra {} */\n'.format(regra.id))
    f.write(T_ANTECEDENTE.format(regra.id, gera_antecedente(regra.antecedente, norma)))
    f.write(T_CONSEQUENTE.format(regra.id, gera_consequente(regra.consequente, n_entradas)))
    f.write(T_SOMA_NUM.format(regra.id, regra.id))
    f.write(T_SOMA_DEN.format(regra.id))
    f.write('\n')

def gera_codigo(base, arquivo_saida):
    T_INICIO    = 'float inferencia({}) {{\n'
    T_PRE_REGRA = 'float num = 0.0, den = 0.0;\n\n'
    T_POS_REGRA = 'return num / den;\n}\n'

    atribui_identificadores(base)

    with open(arquivo_saida, 'w') as f:
        f.write(T_BIBLIOTECA)

        entradas_str = ['float entrada_{}'.format(e.id) for e in base.entradas]
        f.write(T_INICIO.format(', '.join(entradas_str)))

        for entrada in base.entradas:
            gera_entrada(entrada, f)

        f.write(T_PRE_REGRA)

        for regra in base.regras:
            gera_regra(regra, base.norma, len(base.entradas), f)

        f.write(T_POS_REGRA)
