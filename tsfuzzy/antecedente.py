"""
Consequentes (se "antecedente" então "consequente").
"""
class Antecedente:
    def __init__(self, esquerda, direita, tipo):
        self.esquerda = esquerda
        self.direita = direita
        self.tipo = tipo
        
    def __call__(self, norma):
        """
        Avalia o antecedente utilizando o objeto `norma` como normas T e S.
        """
        return norma(self.tipo, self.esquerda(norma), self.direita(norma))
        
    def __or__(self, outro):
        return Antecedente(self, outro, 'S')
        
    def __and__(self, outro):
        return Antecedente(self, outro, 'T')