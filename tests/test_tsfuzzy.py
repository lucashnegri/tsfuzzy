"""
Testes unitários.
"""

import unittest
import random
import numpy as np
import tsfuzzy.pertinencia as p
import tsfuzzy.norma as norma
import tsfuzzy.consequente as consequente

class TestPertinencia(unittest.TestCase):
    
    def test_trapezoidal(self):
        trap = p.Trapezoidal(1, 3, 6, 9)
        
        self.assertAlmostEqual(trap(0), 0)
        self.assertAlmostEqual(trap(1), 0)
        self.assertAlmostEqual(trap(9), 0)
        
        for i in range(2, 9):
            self.assertLessEqual(trap(i), 1)
            self.assertGreaterEqual(trap(i), 0)

        np.testing.assert_allclose(trap(np.array([0, 1, 9])), np.array([0, 0, 0]))
            
    def test_triangular(self):
        trig = p.Triangular(1, 6, 9)
        self.assertAlmostEqual(trig(0), 0)
        self.assertAlmostEqual(trig(1), 0)
        self.assertAlmostEqual(trig(9), 0)
        
        for i in range(2, 9):
            self.assertLessEqual(trig(i), 1)
            self.assertGreaterEqual(trig(i), 0)
            
class TestNormas(unittest.TestCase):
    
    def avalia_norma(self, t, s):
        for i in range(100):
            a = random.random()
            b = random.random()
            self.assertLessEqual(t(a, b), s(a, b))
    
    def test_minimo_maximo(self):
        self.avalia_norma(norma.minimo, norma.maximo)
            
    def test_produto_soma(self):
        self.avalia_norma(norma.produto_algebrico, norma.soma_probabilistica)
        
    def test_objeto(self):
        mm = norma.MinMax
        self.assertEqual(mm('T', 0.1, 0.9), 0.1)
        self.assertEqual(mm('S', 0.1, 0.9), 0.9)
        
class TestConsequente(unittest.TestCase):
    
    def test_ordem0(self):
        x = np.random.randint(0, 10, size=2)
        p = 5
        fx = p
        self.assertAlmostEqual(consequente.polinomial(valores=x, coefs=5), fx)
    
    def test_ordem1(self):
        x = np.random.randint(0, 10, size=2)
        p = np.random.rand(3)
        fx = p[0] + x[0]*p[1] + x[1]*p[2]
        self.assertAlmostEqual(consequente.polinomial(valores=x, coefs=p), fx)
    
    def test_ordem2(self):
        x = np.random.randint(0, 10, size=2)
        p = np.random.rand(5)
        fx = p[0] + x[0]*p[1] + x[1]*p[2] + x[0]**2*p[3] + x[1]**2*p[4]

        for i in range(10):
            self.assertAlmostEqual(consequente.polinomial(valores=x, coefs=p), fx)
        
        poli = consequente.Polinomial(p)
        self.assertAlmostEqual(poli(0.5, x), 0.5*fx)
            
if __name__ == '__main__':
    random.seed(42)
    unittest.main()
