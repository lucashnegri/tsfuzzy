"""
Testes unitários.
"""

import unittest
import random
import numpy as np
from tsfuzzy import antecedente as A, pertinencia as P, entrada as E, norma as N

class TestAntecedente(unittest.TestCase):
    def setUp(self):
        self.E1 = E.Entrada()
        self.E2 = E.Entrada()
        self.E3 = E.Entrada()
        
        self.A = P.Triangular(0, 10, 11)
        self.A.entrada = self.E1
        
        self.B = P.Triangular(0, 10, 11)
        self.B.entrada = self.E2
        
        self.C = P.Triangular(0, 10, 11)
        self.C.entrada = self.E3

    def test_simples(self):
        ANT1 = self.A | self.B
        ANT2 = self.A & self.B
        
        self.E1.valor = 10
        self.E2.valor = 0
        self.assertAlmostEqual(ANT1(N.MinMax), 1)
        self.assertAlmostEqual(ANT2(N.MinMax), 0)

    def test_composto(self):
        ANT = self.A & self.B & self.C
        
        self.E1.valor = 10
        self.E2.valor = 10
        self.E3.valor = 10
        self.assertAlmostEqual(ANT(N.MinMax), 1)
        
        self.E1.valor = 10
        self.E2.valor = 0
        self.E3.valor = 10
        self.assertAlmostEqual(ANT(N.MinMax), 0)

    def test_precedencia(self):
        ANT1 =  self.A & (self.B  | self.C)
        ANT2 = (self.A &  self.B) | self.C
        
        self.E1.valor = 0
        self.E2.valor = 0
        self.E3.valor = 10
        self.assertAlmostEqual(ANT1(N.MinMax), 0)
        self.assertAlmostEqual(ANT2(N.MinMax), 1)

if __name__ == '__main__':
    random.seed(42)
    unittest.main()
