import tsfuzzy as fz
from tsfuzzy import gerador

if __name__ == '__main__':
    # código de teste
    servico = fz.Entrada(0, 10)
    servico['ruim'] = fz.Trapezoidal(-1, 0, 4, 6)
    servico['bom'] = fz.Triangular(4, 6, 10)
    servico['excelente'] = fz.Trapezoidal(6, 9, 10, 11)

    comida = fz.Entrada(0, 10)
    comida['rançosa']  = fz.Trapezoidal(-1, 0, 3, 6)
    comida['deliciosa'] = fz.Trapezoidal(6, 9, 10, 11)

    # definição das regras
    base = fz.Base([servico, comida])
    base.adiciona(SE=servico['ruim'] | comida['rançosa'], ENTAO=5)
    base.adiciona(SE=servico['bom'], ENTAO=15)
    base.adiciona(SE=servico['excelente'] | comida['deliciosa'], ENTAO=25)
    
    gerador.gera_codigo(base, "out.c") 
