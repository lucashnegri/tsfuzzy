"""
Avalia a API por meio da implementação do exemplo clássico do cálculo da
gorjeta.
"""

import pickle
import tsfuzzy as fz
from tsfuzzy import utils

def define_sistema():
    """ Definie o sistema de inferência e o salva em arquivo. """

    # configuração das entradas e funções de pertinência
    servico = fz.Entrada(0, 10)
    servico['ruim'] = fz.Trapezoidal(-1, 0, 4, 6)
    servico['bom'] = fz.Triangular(4, 6, 10)
    servico['excelente'] = fz.Trapezoidal(6, 9, 10, 11)

    comida = fz.Entrada(0, 10)
    comida['rançosa']  = fz.Trapezoidal(-1, 0, 3, 6)
    comida['deliciosa'] = fz.Trapezoidal(6, 9, 10, 11)

    # definição das regras
    base = fz.Base([servico, comida])
    base.adiciona(SE=servico['ruim'] | comida['rançosa'], ENTAO=5)
    base.adiciona(SE=servico['bom'], ENTAO=15)
    base.adiciona(SE=servico['excelente'] | comida['deliciosa'], ENTAO=25)

    # salva em arquivo
    with open('gorjeta.pickle', 'wb') as f:
        pickle.dump(base, f, pickle.HIGHEST_PROTOCOL)

def testa_sistema():
    """ Carrega o sistema de inferência de um arquivo, e mostra o mapeamento realizado. """

    with open('gorjeta.pickle', 'rb') as f:
        base = pickle.load(f)

    # mostra as entradas e o mapeamento realizado pelo sistema
    utils.mostra_entrada(base.entradas[0])
    utils.mostra_entrada(base.entradas[1])
    utils.mostra_mapeamento2d(base)

if __name__ == '__main__':
    # exemplifica a serialização do sistema para arquivo
    define_sistema()
    testa_sistema()
